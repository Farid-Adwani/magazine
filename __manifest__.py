# -*- coding: utf-8 -*-
{
    'name': "Magazine",

    'summary': """
        Module qui gere les operations des reservations et publications des pages de magazine
        """,

    'description': """
       Module qui gere les operations des reservations et publications des pages de magazine
    """,

    'author': "BWS",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale','stock'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/magazine.xml',
        'views/page.xml',
        'views/publish_command.xml',
        'views/attachement.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}

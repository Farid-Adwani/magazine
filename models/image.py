# -*- coding: utf-8 -*-

from odoo import models, fields, api

class image(models.Model):
    name='image'
    ordre_publication=fields.Many2one(comodel_name='publish_command',readonly=True)
    file=fields.Binary("Image")

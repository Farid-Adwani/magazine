# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError

class publish_command(models.Model):
    _name= 'publish_command'
    _inherit = 'mail.thread'
    _description='Commande de publication de page'
    ref=fields.Char(readonly=True,compute='reference')
    writer=fields.Many2one(comodel_name='res.partner',string="Ecrivain")
    page=fields.Many2one(comodel_name='product.template',required=True,domain=[('default_code','like','PAGE')])
    release=fields.Integer("Série ° ",required=True)
    texte=fields.Text("Texte de l'article ")
    images=fields.One2many(string="Images",comodel_name="image",inverse_name='ordre_publication')
    fichiers=fields.One2many(string="Fichiers",comodel_name="attachement",inverse_name='ordre_publication')
    customer=fields.Many2one(comodel_name='res.partner',required=True)
    magazine_name=fields.Char('Nom De La Magazine',related='page.magazine.title')
    img_magazine=fields.Binary('Photo De La Magazine',related='page.magazine.image')
    img_page=fields.Binary('Photo',related='page.image_1920')

    def name_get(self):
        result = []
        for rec in  self:
            alias = rec.ref
            result.append((rec.id, alias))
        return result
    def reference(self):
        for rec in self:
            rec.ref="M"+str(rec.page.magazine.id)+"P"+str(rec.page.id)+"C"+str(rec.customer.id)+"S"+str(rec.release);

    @api.model
    def create(self, values):
        test=False;
        sold_pages = 0
        customer = self.env['res.partner'].browse(values['customer'])
        page = self.env['product.template'].browse(values['page'])

        if(values['release']<1 or values['release']>page.magazine.number_release ):

            raise ValidationError(
                'Le numéro de série doit etre entre 1  et '+str(page.magazine.number_release))


        sale_orders=self.env['sale.order.line'].search([('state','=','sale'),('order_partner_id','=',values['customer']),('product_id','=',page.id)])
        if(len(sale_orders)==0):
            raise ValidationError('Attention '+str(customer.name)+' n a pas achetez '
                                    'cette page: '+str(page.name))

        same_order=self.env['publish_command'].search(['&',('page','=',values['page']),('release','=',values['release'])])
        if (len(same_order)==0 and page.default_code !="PAGE MILIEU") or (len(same_order)<(page.magazine.number_pages-2) and page.default_code =="PAGE MILIEU")  :
            for order in sale_orders:
                sold_pages+=order.product_uom_qty
            consumed=self.env['publish_command'].search([('page','=',values['page']),('customer','=',values['customer'])])
            if(sold_pages<=len(consumed)):
                raise ValidationError("Désolé :(\n il faut achetez une autre page -- "+str(page.name)+" -- car vous avez utilisé tous vos exemplaires ")
            res = super(publish_command, self).create(values)
        else:
            raise ValidationError('Désolé :( \n toutes les page :  '+str(page.name)+' sont réservées pour la série '+values['release'])
        return res;




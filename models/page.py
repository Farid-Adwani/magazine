# -*- coding: utf-8 -*-

from odoo import models, fields, api

class page(models.Model):
    _inherit = 'product.template'
    magazine=fields.Many2one(comodel_name='mymagazine')
    published_pages=fields.Integer("Publiées ",compute="published",readonly=True)


    def published(self):
        for rec in self:
            self.published_pages=len(self.env['publish_command'].search([('page','=',rec.id)]))


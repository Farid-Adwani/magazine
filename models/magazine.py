# -*- coding: utf-8 -*-
from odoo.exceptions import ValidationError
from odoo import models, fields, api
from odoo.modules.module import get_module_resource
import base64

class MyMagazine(models.Model):
    _name = 'mymagazine'
    _inherit = 'mail.thread'
    _description='Une magazine qui fait des publicités'

    title = fields.Char(string='Title',required=True,help='This is the name of the magazine ',index=True)
    image=fields.Image(string="Cover",max_width=200,max_height=350,help="This is the cover of the book")
    total_price=fields.Float("Total Prix ",readonly=True,compute='total')
    number_pages=fields.Integer("Nombre Total des Pages", required=True,default=12)
    number_release = fields.Integer("Nombre de séries Par année", required=True, default=12)
    price1=fields.Float("Prix de la Premiere Page",required=True,default=15)
    price2=fields.Float("Prix de la Page au Milieu",required=True,default=10)
    price3=fields.Float("Prix de la dernière Page",required=True,default=15)
    pages=fields.One2many(comodel_name='product.template',inverse_name='magazine')

    def name_get(self):
        result = []
        for book in self:
            alias = book.title.upper() + " --> " + str(book.number_pages)+" pages"
            result.append((book.id,alias))
        return result

    def total(self):
        for mag in self:
            mag.total_price = mag.price1 + mag.price3 + mag.price2 * (mag.number_pages - 2)

    @api.onchange('number_pages')
    def warning(self):
        if ((self.number_pages) < 0):
            return {'warning': {'title': 'Warning',
                                'message': 'The number of pages can not be negatif'}}
        elif ((self.number_pages) <4 and (self.number_pages)>0 ):
            return {'warning': {'title': 'Warning',
                                'message': 'The number of pages is too few'}}

    @api.model
    def create(self, values):
        if(values['price1']<1):
            raise ValidationError(
                'Le prix de la Première page doit etre positif et non null')
        if (values['price3'] < 1):
            raise ValidationError(
                'Le prix de la dernière page doit etre positif et non null')
        if (values['price2'] < 1):
            raise ValidationError(
                'Le prix de la page milieu doit etre positif et non null')
        if (values['number_pages'] < 1):
            raise ValidationError(
                'Attention !!! \n nombre de pages invalide')
        if (values['number_release'] < 1):
            raise ValidationError(
                'Le nombre de séries doit etre positif et non null')
        res =super(MyMagazine, self).create(values);
        #page1
        self.create_page("Première page",values['price1'],1,"page1.jpg",values['number_release'],res)
        # page milieu
        self.create_page("Page milieu",values['price2'],"MILIEU", "page2.jpg", values['number_release']*(values['number_pages']-2),res)
        # page finale
        self.create_page("Dernière page",values['price3'],values['number_pages'],"page3.jpg",values['number_release'],res)
        return res;

    def create_page(self,pre,price,index,image,qty,magazine):
        name=pre+" de "+str(magazine.title)
        ref="PAGE "+str(index)
        map_vals = {'name': name, 'color': 5, 'type': "product", 'list_price': price,
                    'default_code':ref, 'image_1920': self.get_image(image),'magazine':magazine.id}
        page = self.env['product.template'].create(map_vals)
        warehouse = self.env['stock.warehouse'].search([('company_id', '=', self.env.company.id)], limit=1)
        self.env['stock.quant'].with_context(inventory_mode=True).create({
            'product_id': page.id,
            'location_id': warehouse.lot_stock_id.id,
            'inventory_quantity': qty,
        })

    def get_image(self,name):
        with open(get_module_resource('magasine','static/src/img',name),'rb') as f:
            return base64.b64encode(f.read())



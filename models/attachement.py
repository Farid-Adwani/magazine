# -*- coding: utf-8 -*-

from odoo import models, fields, api

class attachement(models.Model):
    name='attachement'
    ordre_publication=fields.Many2one(comodel_name='publish_command',readonly=True)
    file=fields.Binary("Fichier")
